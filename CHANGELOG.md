# Node Store

### v0.2.1
- Realizados ajustes nos modelos de customer e order.

### v0.2.0
- Criado modelo de customer.
- Criado o modelo de order.
- Criado o cadastro de customer.
- Criado a atualização de customer.
- Criado a exclusão de customer.
- Criado a consulta de pedidos.
- Criado o cadastro de pedidos.
- Criado a atualização de pedidos.
- Criado a exclusão de pedidos.

### v0.1.2
- Adicionadas melhorias na validações.
- Aplicado repository pattern.
- Aplicado async await.

### v0.1.1
- Criada primeira versão de rotas crud.
- Adicionada separação para as rotas.
- Adicionado controller para products.
- Adicionado docker-compose para o mongodb.
- Adicionada conexão com o mongodb.
- Adicionado schema para o produto.
- Adicionado cadastro de produtos.
- Adicionada consulta ao cadastro de produtos.
- Adicionada consulta ao cadastro de produtos utilizando slug como parâmetro.
- Adicionada consulta ao cadastro de produtos utilizando o id como parâmetro.
- Adicionada consulta ao cadastro de produtos utilizando a tag como parâmetro.
- Adicionada atualização de produtos.
- Adicionada exclusão de produtos.

### v0.1.0
- Realizada a separação do servidor em uma outra camada.
- Adicionada configuração para o npm start, a partir de agora pode-se inicializar a aplicação executando apenas o comando `npm start`.
- Adicionado suporte ao nodemon.

### v0.0.1
- Setup para teste do ambiente.
- Instalados os pacotes debug, express e http.
- Adicionado servidor web.
- Adicionada função que normaliza a porta http da aplicação.
- Adicionado tratamento de erros.
- Adicionado o debug.