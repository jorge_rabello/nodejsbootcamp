# Node Store

## Aulas do Curso NodeJS 
Playlist: https://www.youtube.com/playlist?list=PLHlHvK2lnJndvvycjBqQAbgEDqXxKLoqn

## Inicializar

Para inicializar esta aplicação execute:

>```shell
> npm start
>```

## Inicializar com nodemon

Para inicializar esta aplicação utilizando o nodemon

1. Faça a instalação do nodemon

>```shell
> npm install -g --force nodemon --save-dev
>```

2. Execute a aplicação utilizando o nodemon

>```shell
> nodemon ./bin/server.js
>```

## MongoDB Docker Container e Mlab

Este projeto conta com docker-compose para inicializar o servidor mongodb.

Partindo do princípio de que você tem o docker-compose instalado execute os comandos abaixo

Inicializar o servidor mongodb

> ```shell
> docker-compose up -d
> ```

Verificar se o container está sendo executado

> ```shell
> docker-compose ps
> ```

>```shell
>       Name                    Command             State                      Ports                    
>-------------------------------------------------------------------------------------------------------
>node-store_mongo_1   docker-entrypoint.sh mongod   Up      0.0.0.0:27017->27017/tcp,:::27017->27017/tcp
>```

Parar o container

> ```shell
> docker-compose down
> ```

Alternativamente você pode utilizar o mlab (https://mlab.com) para criar e deployar sua base de dados MongoDB 

Instruções: https://www.youtube.com/watch?v=UAp91RCIqoU&list=PLHlHvK2lnJndvvycjBqQAbgEDqXxKLoqn&index=13