'use strict';

const mongoose = require('mongoose');
const Order = mongoose.model('Order');

exports.create = async (data) => {
    var order = new Order(data);
    await order.save();
};

exports.get = async () => {
    const res = await Order
        .find({}, 'number status customer items')
        .populate('customer', 'name email')
        .populate('items.product', 'title price');
    return res;
};

exports.update = async (id, data) => {
    await Order
        .findByIdAndUpdate(id, {
            $set: {
                customer: data.customer,
                status: data.status,
                items: data.items,
            }
        });
};

exports.delete = async (id) => {
    await Order.findOneAndRemove(id);
};