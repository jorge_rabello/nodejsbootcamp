'use strict';

const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

const app = express();
const router = express.Router();

const secrects = require('../secrets/connection.js');

// conecta ao banco de dados
mongoose.connect('mongodb+srv://' + secrects.CREDENTIALS + '@cluster0.b83g4.mongodb.net/nodestore?retryWrites=true&writeConcern=majority', { useNewUrlParser: true, useUnifiedTopology: true });

// carrega os models
const Product = require('./models/product');
const Customer = require('./models/customer');
const Order = require('./models/order');

// carrega as rotas
const indexRoute = require('./routes/index-route');
const productRoute = require('./routes/product-route');
const customerRoute = require('./routes/customer-route');
const orderRoute = require('./routes/order-route');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/', indexRoute);
app.use('/products', productRoute);
app.use('/customers', customerRoute);
app.use('/orders', orderRoute);

module.exports = app;