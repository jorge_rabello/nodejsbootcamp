'use strict';

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/product-repository');

exports.get = async (req, res, next) => {
    try {
        var data = await repository.get();
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.getBySlug = async (req, res, next) => {
    try {
        var data = await repository.getBySlug(req.params.slug);
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.getById = async (req, res, next) => {
    try {
        var data = await repository.getById(req.params.id);
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.getByTag = async (req, res, next) => {
    try {
        var data = await repository.getByTag(req.params.tag);
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.post = async (req, res, next) => {
    let contract = new ValidationContract();

    contract.hasMinLen(req.body.title, 3, 'O campo title deve conter ao menos três caracteres.');
    contract.isRequired(req.body.title, 'O campo title é obrigatório.');

    contract.hasMinLen(req.body.slug, 3, 'O campo slug deve conter ao menos três caracteres.');
    contract.isRequired(req.body.slug, 'O campo slug é obrigatório.');

    contract.hasMinLen(req.body.description, 3, 'O campo description deve conter ao menos três caracteres.');
    contract.isRequired(req.body.description, 'O campo description é obrigatório.');

    contract.hasMinValue(req.body.price, 0.01, 'O produto precisa ter um preço.');
    contract.isRequired(req.body.price, 'O campo price é obrigatório.');

    contract.isRequired(req.body.active, 'O campo active é obrigatório.');

    contract.isRequired(req.body.tags, 'O campo tags é obrigatório.');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.create(req.body);
        res.status(201).send({ message: 'Produto cadastrado com sucesso !' });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.put = async (req, res, next) => {
    let contract = new ValidationContract();

    contract.hasMinLen(req.body.title, 3, 'O campo title deve conter ao menos três caracteres.');
    contract.isRequired(req.body.title, 'O campo title é obrigatório.');

    contract.hasMinLen(req.body.slug, 3, 'O campo slug deve conter ao menos três caracteres.');
    contract.isRequired(req.body.slug, 'O campo slug é obrigatório.');

    contract.hasMinLen(req.body.description, 3, 'O campo description deve conter ao menos três caracteres.');
    contract.isRequired(req.body.description, 'O campo description é obrigatório.');

    contract.hasMinValue(req.body.price, 0.01, 'O produto precisa ter um preço.');
    contract.isRequired(req.body.price, 'O campo price é obrigatório.');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.update(req.params.id, req.body);
        res.status(200).send({ message: 'Produto atualizado com sucesso !' });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.delete = async (req, res, next) => {
    try {
        await repository.delete(req.params.id);
        res.status(204).send();
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};