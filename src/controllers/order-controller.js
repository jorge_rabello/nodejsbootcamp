'use strict';

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/order-repository');
const guid = require('guid');

exports.get = async (req, res, next) => {
    try {
        var data = await repository.get();
        res.status(200).send(data);
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.post = async (req, res, next) => {
    let contract = new ValidationContract();

    contract.isRequired(req.body.customer, 'O campo customer é obrigatório.');
    contract.isRequired(req.body.items, 'O campo items é obrigatório.');
    
    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.create({
            customer: req.body.customer,
            number: guid.raw().substring(0, 6),
            items: req.body.items
        });
        res.status(201).send({ message: 'Pedido cadastrado com sucesso !' });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.put = async (req, res, next) => {
    let contract = new ValidationContract();

    contract.isRequired(req.body.customer, 'O campo customer é obrigatório.');
    contract.isRequired(req.body.status, 'O campo status é obrigatório.');
    contract.isRequired(req.body.items, 'O items quantity é obrigatório.');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.update(req.params.id, req.body);
        res.status(200).send({ message: 'Pedido atualizado com sucesso !' });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.delete = async (req, res, next) => {
    try {
        await repository.delete(req.params.id);
        res.status(204).send();
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};