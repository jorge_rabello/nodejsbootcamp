'use strict';

const ValidationContract = require('../validators/fluent-validator');
const repository = require('../repositories/customer-repository');

exports.post = async (req, res, next) => {
    let contract = new ValidationContract();

    contract.hasMinLen(req.body.name, 3, 'O campo name deve conter ao menos três caracteres.');
    contract.isRequired(req.body.name, 'O campo name é obrigatório.');

    contract.hasMinLen(req.body.email, 3, 'O campo email deve conter ao menos três caracteres.');
    contract.isRequired(req.body.email, 'O campo email é obrigatório.');
    contract.isEmail(req.body.email, 'E-mail inválido.');

    contract.hasMinLen(req.body.password, 8, 'O campo password deve conter ao menos oito caracteres.');
    contract.isRequired(req.body.password, 'O campo password é obrigatório.');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.create(req.body);
        res.status(201).send({ message: 'Cliente cadastrado com sucesso !' });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.put = async (req, res, next) => {
    let contract = new ValidationContract();

    contract.hasMinLen(req.body.email, 3, 'O campo email deve conter ao menos três caracteres.');
    contract.isRequired(req.body.email, 'O campo email é obrigatório.');
    contract.isEmail(req.body.email, 'E-mail inválido.');

    contract.hasMinLen(req.body.password, 8, 'O campo password deve conter ao menos oito caracteres.');
    contract.isRequired(req.body.password, 'O campo password é obrigatório.');

    if (!contract.isValid()) {
        res.status(400).send(contract.errors()).end();
        return;
    }

    try {
        await repository.update(req.params.id, req.body);
        res.status(200).send({ message: 'Cliente atualizado com sucesso !' });
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};

exports.delete = async (req, res, next) => {
    try {
        await repository.delete(req.params.id);
        res.status(204).send();
    } catch (error) {
        res.status(500).send({
            message: 'Falha ao processar a requisição...'
        });
    }
};