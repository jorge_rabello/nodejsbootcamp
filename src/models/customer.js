'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    name: {
        type: String,
        required: [true, 'O campo name é obrigatório.'],
        trim: true
    },
    email: {
        type: String,
        required: [true, 'O campo email é obrigatório.'],
        trim: true,
        index: true,
        unique: true
    },
    password: {
        type: String,
        required: [true, 'O campo password é obrigatório.'],
        trim: true
    }
});

module.exports = mongoose.model('Customer', schema);