'use strict';

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    customer: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Customer',
        required: [true, 'O campo customer é obrigatório.']
    },
    number: {
        type: String,
        required: [true, 'O campo number é obrigatório.'],
        trim: true
    },
    createDate: {
        type: Date,
        required: [true, 'O campo createDate é obrigatório.'],
        default: Date.now
    },
    status: {
        type: String,
        required: [true, 'O campo status é obrigatório.'],
        enum: ['created', 'done'],
        default: 'created'
    },
    items: [{
        quantity: {
            type: Number,
            required: [true, 'O campo quantity é obrigatório.'],
            default: 1
        },
        price: {
            type: Number,
            required: [true, 'O campo price é obrigatório.']
        },
        product: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Product'
        },
    }]
});

module.exports = mongoose.model('Order', schema);